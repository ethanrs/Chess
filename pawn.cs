using System;
using Gtk;

public class Pawn : Piece {

	public Pawn(Tile[][] b, Tile t, char team) : base(b, t, team) {
		if (team == 'w') {
			name = "WP";
		} else if (team == 'b') {
			name = "BP";
		}
	}

	public override bool validMove(Tile src, Tile dest) {
		if (src.x == dest.x) {
			//check forward space
			if (((src.y - 1 == dest.y) && (team == 'w')) ||
				(src.y + 1 == dest.y) && (team == 'b')) {
				return true;
			//check if can pawn can "jump"
			} else if (((src.y - 2 == dest.y) && (src.y == 6) &&
						(!board[src.x][5].occupied) && (team == 'w')) ||
						((src.y + 2 == dest.y) && (src.y == 1) &&
						(!board[src.x][2].occupied) && (team == 'b'))) {
				return true;
			}
		}

		return false;
	}

	public override bool canEat(Tile src, Tile dest) {
		if (board[dest.x][dest.y].getPiece().getTeam() == team) {
			return false;
		}
		if ((src.x - 1 == dest.x) || (src.x + 1 == dest.x)) {
			if ((team == 'w') && (src.y - 1 == dest.y)) {
				return true;
			} else if ((team == 'b') && (src.y + 1 == dest.y)) {
				return true;
			}
		}
		return false;
	}

	//returns whether or not promotion is necessary
	public override bool promote() {
		if ((team == 'w') && (t.y == 0)) {
			return true;
		} else if ((team == 'b') && (t.y == 7)) {
			return true;
		}
		return false;
	}
}
