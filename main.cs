using System;
using Gtk;

namespace Chess {
	class MainClass {
		public static void Main(string[] args) {
			Application.Init();
			Control window = new Control();
			window.Show();
			Application.Run();
		}
	}
}

