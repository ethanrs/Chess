using System;
using Gtk;

public class King : Piece {

	private bool castling;
	private bool castlingDone;
	private Tile rook;
	private Tile newRook;

	public King(Tile[][] b, Tile t, char team) : base(b, t, team) {
		if (team == 'w') {
			name = "WKing";
		} else if (team == 'b') {
			name = "BKing";
		}
		castling = false;
		castlingDone = false;
		rook = null;
		newRook = null;
	}

	public override bool validMove(Tile src, Tile dest) {
		/*
			checks if castling is selected and if possible
			castling is true when selected and possible
			rook is old tile of the rook part of the castling
			newRook is new tile of the rook part of the castling
		*/
		if (!moved) {
			if ((team == 'w') && (dest.x == 2) && (dest.y == 7) &&
				(board[0][7].occupied) && (!board[1][7].occupied) &&
				(!board[2][7].occupied) && (!board[3][7].occupied)) {
				if ((board[0][7].getPiece().getName() == "WR") &&
					(board[0][7].getPiece().moved == false)) {
					moved = true;
					castling = true;
					rook = board[0][7];
					newRook = board[3][7];
					return true;
				}
			} else if ((team == 'w') && (dest.x == 6) && (dest.y == 7) &&
						(board[7][7].occupied) && (!board[6][7].occupied) &&
						(!board[5][7].occupied)) {
				if ((board[7][7].getPiece().getName() == "WR") &&
					(board[7][7].getPiece().moved == false)) {
					moved = true;
					castling = true;
					rook = board[7][7];
					newRook = board[5][7];
					return true;
				}
			} else if ((team == 'b') && (dest.x == 2) && (dest.y == 0) &&
						(board[0][0].occupied) && (!board[1][0].occupied) &&
						(!board[2][0].occupied) && (!board[3][0].occupied)) {
				if ((board[0][0].getPiece().getName() == "BR") &&
					(board[0][0].getPiece().moved == false)) {
					moved = true;
					castling = true;
					rook = board[7][7];
					newRook = board[3][0];
					return true;
				}
			} else if ((team == 'b') && (dest.x == 6) && (dest.y == 0) &&
						(board[7][0].occupied) && (!board[6][0].occupied) &&
						(!board[5][0].occupied)) {
				if ((board[7][0].getPiece().getName() == "BR") &&
					(board[7][0].getPiece().moved == false)) {
					moved = true;
					castling = true;
					rook = board[7][0];
					newRook = board[5][0];
					return true;
				}
			}
		}

		if (((dest.x == src.x) && (dest.y == src.y - 1)) ||
			((dest.x == src.x) && (dest.y == src.y + 1)) ||
			((dest.x == src.x - 1) && (dest.y == src.y)) ||
			((dest.x == src.x + 1) && (dest.y == src.y)) ||
			((dest.x == src.x - 1) && (dest.y == src.y - 1)) ||
			((dest.x == src.x - 1) && (dest.y == src.y + 1)) ||
			((dest.x == src.x + 1) && (dest.y == src.y - 1)) ||
			((dest.x == src.x + 1) && (dest.y == src.y + 1))) {
			moved = true;
			return true;
		}
		return false;
	}

	public override bool canEat(Tile src, Tile dest) {
		if (board[dest.x][dest.y].getPiece().getTeam() == team) {
			return false;
		}
		return validMove(src, dest);
	}

	//moves rook in case of castling
	public override void castle() {
		if (castling) {
			Piece p = rook.release();
			newRook.obtain(p);
			p.move(newRook);
			p.moved = true;
			castlingDone = true;
		}
		castling = false;
	}

	//moves rook back in case that castling results in check
	public override void reverseCastle() {
		if (castlingDone) {
			Piece p = newRook.release();
			rook.obtain(p);
			p.move(rook);
			moved = false;
			p.moved = false;
		}
		castlingDone = false;
	}
}
