using System;
using Gtk;

/*
	contains all necessary objects to run game
	board is all tiles of the game board
	turns alternate starting with white
	each tile in board contain a piece object
*/
public class Control: Gtk.Window {

	private static Tile[][] board;
	private static Tile activated;
	private static int turns;

	public Control(): base (Gtk.WindowType.Toplevel) {

		this.Name = "Chess";
		this.Title = "Chess";
		Table table = new Table(8, 8, true);
		this.Add(table);
		board = new Tile[8][];
		for (int i = 0; i < 8; i++) {
			board[i] = new Tile[8];
			for (int j = 0; j < 8; j++) {
				Button button = new Button();
				table.Attach(button, (uint)i, (uint)i + 1, (uint)j, (uint)j + 1);
				Tile t = new Tile(i, j, button);
				board[i][j] = t;
				button.Show();
				button.Clicked += delegate {
					activate(t);
				};
			}
		}
		table.Show();

		this.DefaultWidth = 500;
		this.DefaultHeight = 500;
		this.Show();
		this.DeleteEvent += delegate {
			Application.Quit();
		};
		init();
		activated = null;
		turns = 0;
	}

	//create pieces for both teams
	private static void init() {
		for (int i = 0; i < 8; i++) {
			Piece p1 = null;
			switch (i) {
				case 0: p1 = new Rook(board, board[i][0], 'b'); break;
				case 1: p1 = new Knight(board, board[i][0], 'b'); break;
				case 2: p1 = new Bishop(board, board[i][0], 'b'); break;
				case 3: p1 = new Queen(board, board[i][0], 'b'); break;
				case 4: p1 = new King(board, board[i][0], 'b'); break;
				case 5: p1 = new Bishop(board, board[i][0], 'b'); break;
				case 6: p1 = new Knight(board, board[i][0], 'b'); break;
				case 7: p1 = new Rook(board, board[i][0], 'b'); break;
			}
			board[i][0].obtain(p1);
			Piece p2 = null;
			switch(i) {
				case 0: p2 = new Rook(board, board[i][7], 'w'); break;
				case 1: p2 = new Knight(board, board[i][7], 'w'); break;
				case 2: p2 = new Bishop(board, board[i][7], 'w'); break;
				case 3: p2 = new Queen(board, board[i][7], 'w'); break;
				case 4: p2 = new King(board, board[i][7], 'w'); break;
				case 5: p2 = new Bishop(board, board[i][7], 'w'); break;
				case 6: p2 = new Knight(board, board[i][7], 'w'); break;
				case 7: p2 = new Rook(board, board[i][7], 'w'); break;
			}
			board[i][7].obtain(p2);
			Piece p3 = new Pawn(board, board[i][1], 'b');
			Piece p4 = new Pawn(board, board[i][6], 'w');
			board[i][1].obtain(p3);
			board[i][6].obtain(p4);	
		}
	}

	//event handler for click event for any button
	private static void activate(Tile t) {
		//if no piece has been selected, select clicked piece
		if ((activated == null) && (t.occupied)) {
			if (((turns % 2 == 0) && (t.getPiece().getTeam() == 'w')) ||
				((turns % 2 == 1) && (t.getPiece().getTeam() == 'b'))) {
				activated = t;
			}
		//move selected piece if possible
		} else if (activated != null) {
			//test if piece can move to empty space or can eat selected opposing piece
			if (((!t.occupied) && (activated.getPiece().validMove(activated, t))) ||
				((t.occupied) && (activated.getPiece().canEat(activated, t)))) {

				//move piece (tile and piece refer to each other)
				Piece p = activated.release();
				t.obtain(p);
				p.move(t);

				//moves rook if trying to castle
				if ((p.getName() == "WKing") || (p.getName() == "BKing")) {
					p.castle();
				}

				//promoted pawn to queen if at end of board
				bool promoted = false;
				if ((p.getName() == "WP") || (p.getName() == "BP")) {
					if (p.promote()) {
						promoted = true;
						p = t.release();
						char team = p.getTeam();
						p = new Queen(board, t, team);
						t.obtain(p);
					}
				}

				//checks if move results in check for own team
				if (inCheck(t.getPiece().getTeam())) {
					//undo the move if putting yourself in check
					t.release();
					activated.obtain(p);
					p.move(activated);
					if ((p.getName() == "WKing") || (p.getName() == "BKing")) {
						p.reverseCastle();
					}
					if ((p.getName() == "WP") || (p.getName() == "BP")) {
						if (promoted) {
							p = t.release();
							char team = p.getTeam();
							p = new Pawn(board, activated, team);
							t.obtain(p);
						}
					}
				} else {
					//if not undoing move, confirm move by incrementing turns
					turns++;
				}
			}
			activated = null;
		}
	}

	private static bool inCheck(char team) {
		int x = 0;
		int y = 0;

		//find position of king for given team
		for (int i = 0; i < 8; i++) {
 			for (int j = 0; j < 8; j++) {
				if (board[i][j].occupied) {
					if ((board[i][j].getPiece().getName() == "BKing") && (team == 'b')) {
						x = i;
						y = j;
					}
					if ((board[i][j].getPiece().getName() == "WKing") && (team == 'w')) {
						x = i;
						y = j;
					}
				}
			}
		}

		//iterate through all pieces and test if piece can eat king in current position
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				if (board[i][j].occupied) {
					if ((board[i][j].getPiece().canEat(board[i][j], board[x][y])) &&
						(board[i][j].getPiece().getTeam() != team)) {
						return true;
					}
				}
			}
		}
		return false;
	}
}
