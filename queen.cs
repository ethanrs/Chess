using System;
using Gtk;

public class Queen : Piece {

	public Queen(Tile[][] b, Tile t, char team) : base(b, t, team) {
		if (team == 'w') {
			name = "WQ";
		} else if (team == 'b') {
			name = "BQ";
		}
	}

	public override bool validMove(Tile src, Tile dest) {
		if ((dest.y < src.y) && (dest.x < src.x)) {
			for (int i = 1; i < (src.y - dest.y) + 1; i++) {
				if ((src.x - i < 0) || (src.y - i < 0)) {
					return false;
				}
				if (board[src.x - i][src.y - i] == dest) {
					return true;
				} else if (board[src.x - i][src.y - i].occupied) {
					return false;
				}
			}
		} else if ((dest.y < src.y) && (dest.x > src.x)) {
			for (int i = 1; i < (src.y - dest.y) + 1; i++) {
				if ((src.x + i > 7) || (src.y - i < 0)) {
					return false;
				}
				if (board[src.x + i][src.y - i] == dest) {
					return true;
				} else if (board[src.x + i][src.y - i].occupied) {
					return false;
				}
			}
		} else if ((dest.y > src.y) && (dest.x < src.x)) {
			for (int i = 1; i < (dest.y - src.y) + 1; i++) {
				if ((src.x - i < 0) || (src.y + i > 7)) {
					return false;
				}
				if (board[src.x - i][src.y + i] == dest) {
					return true;
				} else if (board[src.x - i][src.y + i].occupied) {
					return false;
				}
			}
		} else if ((dest.y > src.y) && (dest.x > src.x)) {
			for (int i = 1; i < (dest.y - src.y) + 1; i++) {
				if ((src.x + i > 7) || (src.y + i > 7)) {
					return false;
				}
				if (board[src.x + i][src.y + i] == dest) {
					return true;
				} if (board[src.x + i][src.y + i].occupied) {
					return false;
				}
			}
		} else if ((src.x == dest.x) && (src.y < dest.y)) {
			for (int i = 1; i < (dest.y - src.y) + 1; i++) {
				if (src.y + i > 7) {
					return false;
				}
				if (board[src.x][src.y + i] == dest) {
					return true;
				} else if (board[src.x][src.y + i].occupied) {
					return false;
				}
			}
		} else if ((src.x == dest.x) && (src.y > dest.y)) {
			for (int i = 1; i < (src.y - dest.y) + 1; i++) {
				if (src.y - i < 0) {
					return false;
				}
				if (board[src.x][src.y - i] == dest) {
					return true;
				} else if (board[src.x][src.y - i].occupied) {
					return false;
				}
			}
		} else if ((src.y == dest.y) && (src.x < dest.x)) {
			for (int i = 1; i < (dest.x - src.x) + 1; i++) {
				if (src.x + i > 7) {
					return false;
				}
				if (board[src.x + i][src.y] == dest) {
					return true;
				} else if (board[src.x + i][src.y].occupied) {
					return false;
				}
			}
		} else if ((src.y == dest.y) && (src.x > dest.x)) {
			for (int i = 1; i < (src.x - dest.x) + 1; i++) {
				if (src.x - i < 0) {
					return false;
				}
				if (board[src.x - i][src.y] == dest) {
					return true;
				} else if (board[src.x - i][src.y].occupied) {
					return false;
				}
			}
		}
		return false;
	}

	public override bool canEat(Tile src, Tile dest) {
		if (board[dest.x][dest.y].getPiece().getTeam() == team) {
			return false;
		}
		return validMove(src, dest);
	}
}
