using System;
using Gtk;

public abstract class Piece {

	protected Tile[][] board;
	protected Tile t;
	protected string name;
	protected char team;
	public bool moved;

	public Piece(Tile[][] b, Tile pos, char c) {
		t = pos;
		team = c;
		board = b;
		moved = false;
	}

	public string getName() {
		return name;
	}

	public char getTeam() {
		return team;
	}

	public void move(Tile newSpace) {
		t = newSpace;
	}

	public abstract bool validMove(Tile src, Tile dest);
	public abstract bool canEat(Tile src, Tile dest);
	public virtual void castle() {}
	public virtual void reverseCastle() {}
	public virtual bool promote() {return false;}
}
