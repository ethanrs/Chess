using System;
using Gtk;

public class Tile {	

	private Button b;
	private Piece p;
	public int x;
	public int y;
	public bool occupied;

	public Tile(int i, int j, Button btn) {
		x = i;
		y = j;
		b = btn;
		p = null;
		occupied = false;
	}

	public void obtain(Piece x) {
		p = x;
		b.Label = p.getName();
		occupied = true;
	}

	public Piece release() {
		b.Label = "";
		Piece temp = p;
		p = null;
		occupied = false;
		return temp;
	}

	public Piece getPiece() {
		return p;
	}
}
