using System;
using Gtk;

public class Rook : Piece {

	public Rook(Tile[][] b, Tile t, char team) : base(b, t, team) {
		if (team == 'w') {
			name = "WR";
		} else if (team == 'b') {
			name = "BR";
		}
	}

	public override bool validMove(Tile src, Tile dest) {
		if ((src.x == dest.x) && (src.y < dest.y)) {
			for (int i = 1; i < (dest.y - src.y) + 1; i++) {
				if (src.y + i > 7) {
					return false;
				}
				if (board[src.x][src.y + i] == dest) {
					moved = true;	
					return true;
				} else if (board[src.x][src.y + i].occupied) {
					return false;
				}
			}
		} else if ((src.x == dest.x) && (src.y > dest.y)) {
			for (int i = 1; i < (src.y - dest.y) + 1; i++) {
				if (src.y - i < 0) {
					return false;
				}
				if (board[src.x][src.y - i] == dest) {
					moved = true;	
					return true;
				} else if (board[src.x][src.y - i].occupied) {
					return false;
				}
			}
		} else if ((src.y == dest.y) && (src.x < dest.x)) {
			for (int i = 1; i < (dest.x - src.x) + 1; i++) {
				if (src.x + i > 7) {
					return false;
				}
				if (board[src.x + i][src.y] == dest) {
					moved = true;
					return true;
				} else if (board[src.x + i][src.y].occupied) {
					return false;
				}
			}
		} else if ((src.y == dest.y) && (src.x > dest.x)) {
			for (int i = 1; i < (src.x - dest.x) + 1; i++) {
				if (src.x - i < 0) {
					return false;
				}
				if (board[src.x - i][src.y] == dest) {
					moved = true;
					return true;
				} else if (board[src.x - i][src.y].occupied) {
					return false;
				}
			}
		}
		return false;
	}

	public override bool canEat(Tile src, Tile dest) {
		if (board[dest.x][dest.y].getPiece().getTeam() == team) {
			return false;
		}
		return validMove(src, dest);
	}
}
