using System;
using Gtk;

public class Knight : Piece {

	public Knight(Tile[][] b, Tile t, char team) : base(b, t, team) {
		if (team == 'w') {
			name = "WK";
		} else if (team == 'b') {
			name = "BK";
		}
	}

	public override bool validMove(Tile src, Tile dest) {
		if (((dest.y == src.y - 1) && (dest.x == src.x - 2)) ||
			((dest.y == src.y + 1) && (dest.x == src.x - 2)) ||
			((dest.y == src.y - 1) && (dest.x == src.x + 2)) ||
			((dest.y == src.y + 1) && (dest.x == src.x + 2)) ||
			((dest.y == src.y - 2) && (dest.x == src.x - 1)) ||
			((dest.y == src.y + 2) && (dest.x == src.x - 1)) ||
			((dest.y == src.y - 2) && (dest.x == src.x + 1)) ||
			((dest.y == src.y + 2) && (dest.x == src.x + 1))) {
			return true;
		}
		return false;
	}

	public override bool canEat(Tile src, Tile dest) {
		if (board[dest.x][dest.y].getPiece().getTeam() == team) {
			return false;
		}
		return validMove(src, dest);
	}
}
